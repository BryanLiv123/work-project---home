#include "screenShot.h"

int counter = 0; //Number of photo

void screenshot(POINT a, POINT b)
{
	// copy screen to bitmap

	HDC     hScreen = GetDC(NULL);
	HDC     hDC = CreateCompatibleDC(hScreen);
	HBITMAP hBitmap = CreateCompatibleBitmap(hScreen, abs(b.x - a.x), abs(b.y - a.y));
	HGDIOBJ old_obj = SelectObject(hDC, hBitmap);
	BOOL    bRet = BitBlt(hDC, 0, 0, abs(b.x - a.x), abs(b.y - a.y), hScreen, a.x, a.y, SRCCOPY);

	// save bitmap to file

	CImage image;
	std::string path = "C:/Users/user/Pictures/BMP/ " + std::to_string(counter) + " .bmp";
	image.Attach(hBitmap);
	image.Save((path).c_str(), Gdiplus::ImageFormatBMP);

	// clean up
	SelectObject(hDC, old_obj);
	DeleteDC(hDC);
	ReleaseDC(NULL, hScreen);
	DeleteObject(hBitmap);
	counter++;
}

int screenShotFunc()
{
	POINT a, b;
	a.x = 0;
	a.y = 0;

	b.x = 1900;
	b.y = 1000;

	while (TRUE)
	{
		screenshot(a, b);
		Sleep(MINUTE);
	}
}