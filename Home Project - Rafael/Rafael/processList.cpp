#include "processList.h"

std::vector<std::string> processVec; //vector of all processes names

/*
Thread function that every 60 seconds writes new data to the file

Input: None
Output: None
*/
void threadFunc()
{
	Sleep(MINUTE);
	LogFile file;
	file.shared_writing("---- Processes ----", processVec);
	processVec.clear(); //Clear the vector for new data
}

void processListFunc()
{
	while (true)
	{
		GetProcessList();
	}
}

BOOL GetProcessList()
{
	std::thread timer(threadFunc); //timer thread and writing to file
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	pe32.dwSize = sizeof(PROCESSENTRY32); // Set the size of the structure before using it.
	
	if (!Process32First(hProcessSnap, &pe32))// Retrieve information about the first process, and exit if unsuccessful
	{
		CloseHandle(hProcessSnap);  // clean the snapshot object
		return FALSE;
	}

	// Now walk the snapshot of processes 
	do
	{
		std::string str(pe32.szExeFile);

		processVec.push_back(str); //Insert process name to the vector

	} while (Process32Next(hProcessSnap, &pe32));

	timer.join();
	CloseHandle(hProcessSnap);
	return TRUE;
}