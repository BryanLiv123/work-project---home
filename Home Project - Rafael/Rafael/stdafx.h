#pragma once

#pragma comment(lib, "Shlwapi.lib")//Library needed by Linker to check file existance
#pragma comment(lib, "advapi32.lib")
#pragma comment(lib, "netapi32.lib")

#define MINUTE 60000 // 60,000 miliseconds = one minute
#define PATH "C:/Users/user/Pictures/Data.txt" //path to the data file location
#define MAX_NAME 256

#include <Windows.h>
#include <Shlwapi.h> 
#include <ctype.h>
#include <atlimage.h> 
#include <thread>
#include <fstream>
#include <string>
#include <tchar.h>
#include <tlhelp32.h>
#include <process.h>
#include <atlstr.h>
#include <Lmcons.h>
#include <vector>
#include <mutex>
#include <cstdlib>
#include <LM.h>
#include "LogFile.h"