#pragma once

#include "stdafx.h"

class LogFile //Class for thread management about writing to the data file
{
public:
	LogFile();
	~LogFile();
	void shared_writing(std::string msg, std::vector<std::string> groupsSet);

private:
	std::mutex _m_mutex;
	std::ofstream _file;
};

extern LogFile file; //class object that the threads use to write to the data file