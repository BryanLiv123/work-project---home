#include "groups.h"

std::vector<std::string> groupsVec; //vector of groups names

void threadWriteFunc() //Timer thread function
{
	Sleep(MINUTE); //Wait for a minute
	LogFile file;
	file.shared_writing("---- User groups ----", groupsVec); //Calling the write to file function
	groupsVec.clear(); //Clear the vector for new data
}

void groupsFunc()
{
	while (true)
	{
		getUserGroups();
	}
}

void getUserGroups()
{
	std::thread timerThread(threadWriteFunc); //timer thread and writing to file
	wchar_t user[MAX_NAME];
	DWORD size = sizeof(user) / sizeof(user[0]);
	LPBYTE buffer;
	DWORD entries, total_entries;

	GetUserNameW(user, &size); //get the user name

	NetUserGetLocalGroups(NULL, user, 0, LG_INCLUDE_INDIRECT, &buffer, MAX_PREFERRED_LENGTH, &entries, &total_entries); //get all the groups that the user are member of

	LOCALGROUP_USERS_INFO_0 *groups = (LOCALGROUP_USERS_INFO_0*)buffer;
	for (DWORD i = 0; i < entries; i++)
	{
		//Convert LPWSTR to string
		std::wstring ws(groups[i].lgrui0_name);
		std::string groupName = std::string(ws.begin(), ws.end());

		groupsVec.push_back(groupName); //Add the group name to the vector
	}

	//Clean up
	NetApiBufferFree(buffer);
	timerThread.join();
}