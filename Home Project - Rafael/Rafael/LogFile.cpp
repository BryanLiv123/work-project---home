#include "LogFile.h"

LogFile::LogFile()
{
	this->_file.open(PATH, std::fstream::app); //opens file for writing
}

LogFile::~LogFile()
{
	this->_file.close(); //Close file
}

/*
Function that setting a lock and writing to the file

Input: msg - what kind of data : processes or groups... , vec - data to write
Output - None
*/
void LogFile::shared_writing(std::string msg, std::vector<std::string> vec)
{
	std::lock_guard<std::mutex> locker(this->_m_mutex); //Lock
	 
	this->_file << msg;
	this->_file << std::endl;
	for (std::vector<std::string>::iterator it = vec.begin(); it != vec.end(); ++it)
	{
		this->_file << it->data();
		this->_file << std::endl;
	}

	this->_file << std::endl;
}