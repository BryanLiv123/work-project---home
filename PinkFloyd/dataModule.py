def getAllAlbums(): #Choice 1
    with open("Pink_Floyd_DB.txt") as file: #Open the file
        albumNames = "-----Albums List-----\n" #string that will storage all albums names
        for line in file:
            if line.startswith("#"):
                endIndex = line.index(':')
                name = line[1:endIndex]
                albumNames += name + '\n'
    file.close()
    return albumNames[:-1] #Delete the last '\n'

def getSongsInAlbum(albumName): #Choice 2
    with open("Pink_Floyd_DB.txt") as file: #Open the file
        songsNames = "-----Songs List-----\n" #string that will storage all songs names
        found = False #if the album was found
        for line in file:
            if line.startswith('#' + albumName):
                found = True
                line = next(file) #get next line
            if found:
                while not line.startswith('#'):
                    if(line.startswith("*")):
                        endIndex = line.index(':')
                        name = line[1:endIndex]
                        songsNames += name + '\n'
                    try:
                        line = next(file) #get next line
                    except StopIteration:
                        break
                found = False
                file.close()
                return songsNames[:-1] #Delete the last '\n'
        return "Album not found..."
    
def getSongLength(songName): #Choice 3
    with open("Pink_Floyd_DB.txt") as file: #Open the file
        for line in file:
            if songName in line and line.startswith('*'):
                line = line.split('::')
                file.close()
                return("Song Length: " + line[2])
        return "Song not found..."

def getLyrics(songName): #Choice 4
    with open("Pink_Floyd_DB.txt") as file: #Open the file
        lyrics = "-----Lyrics-----\n"
        for line in file:
            if songName in line and '*' in line:
                line = line.split('::')
                lyrics = line[3]
                line = next(file)
                while not line.startswith('*') and not line.startswith('#'):
                    lyrics += line
                    try:
                        line = next(file)
                    except StopIteration:
                        break
                file.close()
                return lyrics
        return "Song not found..."

def getAlbumNameFromSong(songName): #Choice 5
    albumList = getAllAlbums()
    for album in albumList:
        if songName in getSongsInAlbum(album):
            return album
    return "Song not found"

def getSongNameByWord(word): #Choice 6
    with open("Pink_Floyd_DB.txt") as file: #Open the file
        songsNames = "-----Songs List-----\n"
        for line in file:
            if line.startswith('*'):
                line = line.split('::')
                if word.lower() in line[0].lower():
                    songsNames += line[0][1:] + '\n'
        file.close()
        if songsNames != "-----Songs List-----\n":
            return songsNames
        return "Song not found..."

def getSongNameByLyrics(word): #Choice 7
    albumList = getAllAlbums().split('\n')
    albumList.remove('-----Albums List-----')
    songsNames = "-----Songs List-----\n"
    for album in albumList:
        songsList = getSongsInAlbum(album).split('\n')
        songsList.remove('-----Songs List-----')
        for song in songsList:
            if word.lower() in getLyrics(song).lower():
                songsNames += song + '\n'
    if songsNames != "-----Songs List-----\n":
        return songsNames
    return "Song not found..."



#print(getAllAlbums())
#print(getSongsInAlbum("The Piper At The Gates Of Dawn"))
#print(getSongLength("Chapter 24"))
#print(getLyrics("Astronomy Domine"))
#print(getAlbumNameFromSong("The Thin Ice"))
#print(getSongNameByWord("bA"))
#print(getSongNameByLyrics("witcH"))