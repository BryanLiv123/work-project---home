import socket
SERVER_IP = "127.0.0.1"
SERVER_PORT = 9999

EXIT = '8'

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connecting to remote computer 9999
server_address = (SERVER_IP, SERVER_PORT)
sock.connect(server_address)

def getServerMsg():
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    print(server_msg)

def sendToServer(choice):
    if choice == '1':
        msg = choice
        sock.sendall(msg.encode())
        getServerMsg()
    if choice == '2':
        albumName = input("Please type the album name: ")
        msg = choice + ':' + albumName
        sock.sendall(msg.encode())
        getServerMsg()
    if choice == '3':
        songName = input("Please type the song name: ")
        msg = choice + ':' + songName
        sock.sendall(msg.encode())
        getServerMsg()
    if choice == '4':
        songName = input("Please type the song name: ")
        msg = choice + ':' + songName
        sock.sendall(msg.encode())
        getServerMsg()
    if choice == '5':
        songName = input("Please type the song name: ")
        msg = choice + ':' + songName
        sock.sendall(msg.encode())
        getServerMsg()
    if choice == '6' or choice == '7':
        word = input("Please type the word for search: ")
        msg = choice + ':' + word
        sock.sendall(msg.encode())
        getServerMsg()
    if choice == '8':
        msg = choice
        sock.sendall(msg.encode())

def printMenu():
    print("1 - Get list of all albums")
    print("2 - Get list of all songs names in given album")
    print("3 - Get length of given song")
    print("4 - Get lyrics of given song")
    print("5 - Get album name that the given song belong to")
    print("6 - Get list of all songs names that contain given word")
    print("7 - Get list of all songs that have a specific word in their lyrics")
    print("8 - Exit")

def main():

    welcomeMSG = True
    userChoice = 0

    while userChoice != EXIT:
        if welcomeMSG: #welcome message
            # Receiving data from the server
            getServerMsg()
            welcomeMSG = False
        print("\n")

        printMenu() #print menu

        userChoice = input("Your choice: ")
        sendToServer(userChoice)
    # Closing the socket
    sock.close()


if __name__ == "__main__":
    main()
