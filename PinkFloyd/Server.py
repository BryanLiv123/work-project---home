import socket
import dataModule
LISTEN_PORT = 9999
EXIT = '8'

#Create a TCP/IP socket
listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Binding to local port 9999
server_address = ('', LISTEN_PORT)
listening_sock.bind(server_address)

def main():
    userChoice = 0
    while True:
        # Listen for incoming connections
        listening_sock.listen(1)
        # Create a new conversation socket
        client_soc, client_address = listening_sock.accept()
        # Sending welcome msg to client
        msg = "Welcome to Pink Floyd Data Server!!"
        client_soc.sendall(msg.encode())
        # Receiving data from the client
        userChoice = client_soc.recv(1024)
        userChoice = userChoice.decode()
        while userChoice != EXIT:
            if userChoice.startswith('1'):
                client_soc.sendall(dataModule.getAllAlbums().encode())
            if userChoice.startswith('2'):
                client_soc.sendall(dataModule.getSongsInAlbum(userChoice[userChoice.index(':') + 1:]).encode())
            if userChoice.startswith('3'):
                client_soc.sendall(dataModule.getSongLength(userChoice[userChoice.index(':') + 1:]).encode())
            if userChoice.startswith('4'):
                client_soc.sendall(dataModule.getLyrics(userChoice[userChoice.index(':') + 1:]).encode())
            if userChoice.startswith('5'):
                client_soc.sendall(dataModule.getAlbumNameFromSong(userChoice[userChoice.index(':') + 1:]).encode())
            if userChoice.startswith('6'):
                client_soc.sendall(dataModule.getSongNameByWord(userChoice[userChoice.index(':') + 1:]).encode())
            if userChoice.startswith('7'):
                client_soc.sendall(dataModule.getSongNameByLyrics(userChoice[userChoice.index(':') + 1:]).encode())
            # Receiving data from the client
            userChoice = client_soc.recv(1024)
            userChoice = userChoice.decode()
        # Closing the conversation socket
        client_soc.close()
    # Closing the listening socket
    listening_sock.close()

if __name__ == "__main__":
    main()